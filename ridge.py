import numpy as np
from numpy.linalg import solve

class RidgeRegression:
    def __init__(self, lambda_value):
        self.lambda_value = lambda_value
        self.w = None

    def fit(self, X, y):
        X = np.array(X)
        y = np.array(y)
        X_t = np.transpose(X)
        X_t_X = X_t.dot(X)
        I = np.identity(len(X_t_X))
        I[0][0]=0 #Recall that the regularization acts over all the model parameters except for
        lambda_I = (self.lambda_value) * I 
        self.w = solve(X_t_X + lambda_I, X_t.dot(y))

    def predict(self, X):
        return np.dot(X, self.w)

    def loss_function(self, X, y):
        N = len(y)
        residuals = y - np.dot(X, self.w)
        return np.dot(residuals.T, residuals) / N + self.L2_regularization()

    def L2_regularization(self):
        if self.w is None:
            return 0
        return self.lambda_value * np.sum(self.w[1:] ** 2)
