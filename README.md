# Projet de Régression Ridge

Ce dépôt contient les ressources nécessaires pour l'implémentation d'une régression ridge à partir de zéro, y compris un script Python, un notebook Jupyter pour les tests, et un rapport détaillant les résultats obtenus. Ce projet vise à approfondir la compréhension de la régression linéaire avec régularisation L2 et à explorer l'efficacité de l'algorithme sur le dataset des 100 mètres des Jeux Olympiques.

## Structure du Dépôt

- `ridge.py` : Script Python contenant l'implémentation complète de la régression ridge.
- `experiment.ipynb` : Notebook Jupyter utilisé pour tester l'implémentation avec le dataset des 100 mètres des Jeux Olympiques.

## Objectifs du Projet

1. **Compréhension Approfondie** : Maîtriser l'implémentation de la régression ridge et comprendre l'importance de la régularisation L2.
2. **Sélection de Modèle** : Renforcer les connaissances sur la sélection du paramètre de régularisation λ et les techniques de validation.
3. **Expérience Pratique** : Développer des compétences pratiques dans l'utilisation de Python et des outils de programmation pour l'apprentissage automatique.

## Installation et Utilisation

1. Clonez ce dépôt sur votre système.
2. Assurez-vous que les dépendances nécessaires sont installées, notamment Python et les bibliothèques nécessaires pour exécuter Jupyter.
3. Exécutez le notebook `experiment.ipynb` pour observer les expérimentations et les analyses réalisées.

## Documentation

Le fichier `ridge.py` inclut des commentaires détaillés pour faciliter la compréhension des mécanismes de l'implémentation de la régression ridge. Le notebook `experiment.ipynb` guide l'utilisateur à travers les étapes de test et de validation de l'implémentation.

## Rapport de Projet

Un rapport d'une page est inclus pour expliquer en détail la stratégie utilisée pour l'implémentation de la régression ridge, les performances sur le dataset des 100 mètres des Jeux Olympiques, et le choix du paramètre λ. Ce rapport peut également contenir une comparaison avec l'implémentation de scikit-learn.

## Contribution

Nous encourageons les contributions pour améliorer l'implémentation et la documentation. Veuillez suivre les lignes directrices spécifiées dans `CONTRIBUTING.md` pour vos contributions.

